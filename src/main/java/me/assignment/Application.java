package me.assignment;

import java.text.SimpleDateFormat;
import java.util.Locale;

import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.security.servlet.SecurityAutoConfiguration;
import org.springframework.context.annotation.Bean;

import lombok.extern.log4j.Log4j2;
import me.assignment.db.entity.OrdersEntity;
import me.assignment.db.entity.UsersEntity;
import me.assignment.db.repository.OrdersRepository;
import me.assignment.db.repository.UsersRepository;

@SpringBootApplication(exclude = { SecurityAutoConfiguration.class })
@Log4j2
public class Application {

	public static void main(String[] args) {
		SpringApplication.run(Application.class, args);
	}

	@Bean
	public CommandLineRunner initData(UsersRepository repository,OrdersRepository repository2) {
		return (args) -> {
			log.debug("==################ INIT DB DATA ################==");
			SimpleDateFormat df = new SimpleDateFormat("dd/MM/yyyy", new Locale("en", "US"));
			UsersEntity user = new UsersEntity();
			user.setUsername("john.doe");
			user.setName("john");
			user.setSurname("doe");
			user.setPassword("password");
			user.setDateOfBirth(df.parse("15/01/1985"));
			repository.save(user);
			
			OrdersEntity order1 = new OrdersEntity();
			order1.setBookId(1L);
			order1.setUsernameOrder(user);
			repository2.save(order1);
			OrdersEntity order2 = new OrdersEntity();
			order2.setBookId(4L);
			order2.setUsernameOrder(user);
			repository2.save(order2);
		};
	}

}
