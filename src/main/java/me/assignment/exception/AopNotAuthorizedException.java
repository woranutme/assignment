package me.assignment.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.UNAUTHORIZED)
public class AopNotAuthorizedException extends RuntimeException {

	/**
	 * 
	 */
	private static final long serialVersionUID = 2163936097404461436L;

	public AopNotAuthorizedException() {
		super();
		// TODO Auto-generated constructor stub
	}

	public AopNotAuthorizedException(String message, Throwable cause, boolean enableSuppression,
			boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
		// TODO Auto-generated constructor stub
	}

	public AopNotAuthorizedException(String message, Throwable cause) {
		super(message, cause);
		// TODO Auto-generated constructor stub
	}

	public AopNotAuthorizedException(String message) {
		super(message);
		// TODO Auto-generated constructor stub
	}

	public AopNotAuthorizedException(Throwable cause) {
		super(cause);
		// TODO Auto-generated constructor stub
	}
	
}
