package me.assignment.controller;

import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import lombok.extern.log4j.Log4j2;
import me.assignment.controller.bean.CreateUserReq;
import me.assignment.controller.bean.LoginReq;
import me.assignment.controller.bean.LoginResp;
import me.assignment.controller.bean.OrdersReq;
import me.assignment.controller.bean.OrdersResp;
import me.assignment.controller.bean.UserOrderResp;
import me.assignment.service.ExternalPubisherService;
import me.assignment.service.OrdersService;
import me.assignment.service.UserTokenService;
import me.assignment.service.UsersService;
import me.assignment.service.impl.bean.ExternalPubisherBookBean;
import me.assignment.utils.HttpRequestUtil;

@RestController
@Log4j2
public class BookStoreController {

	@Autowired
	private HttpRequestUtil httpRequestUtil;
	
	@Autowired
	private UserTokenService userTokenService;
	
	@Autowired
	private UsersService usersService;
	
	@Autowired
	private OrdersService ordersService;
	
	@Autowired
	private ExternalPubisherService externalPubisherService;
	
	@RequestMapping(path = "/login", method = RequestMethod.POST
			, produces = { MediaType.APPLICATION_JSON_UTF8_VALUE }
			, consumes = { MediaType.APPLICATION_JSON_UTF8_VALUE })
	@ResponseBody
	public LoginResp login(@RequestBody LoginReq req) throws Exception {
		log.debug("-- begin : login --");
		return LoginResp.builder()
				.date(new Date())
				.token(userTokenService.saveToken(req.getUsername(), req.getPassword()))
				.build();
	}
	
	@RequestMapping(path = "/users", method = RequestMethod.GET
			, produces = { MediaType.APPLICATION_JSON_UTF8_VALUE }
			, consumes = { MediaType.APPLICATION_JSON_UTF8_VALUE })
	@ResponseBody
	public UserOrderResp secureGetUsers() throws Exception {
		log.debug("-- begin : secureGetUsers --");
		return usersService.getUserDetail(httpRequestUtil.getHttpHeaderUserId());
	}
	
	@RequestMapping(path = "/users", method = RequestMethod.DELETE
			, produces = { MediaType.APPLICATION_JSON_UTF8_VALUE }
			, consumes = { MediaType.APPLICATION_JSON_UTF8_VALUE })
	@ResponseBody
	public void secureDeleteUsers() throws Exception {
		log.debug("-- begin : secureDeleteUsers --");
		HttpServletRequest request = httpRequestUtil.getHttpServletRequest();
		String authorization = request.getHeader(HttpRequestUtil.AUTHORIZATION);
		String userId = request.getHeader(HttpRequestUtil.USER_ID);
		usersService.deleteUserDetail(userId, authorization);
	}
	
	@RequestMapping(path = "/users", method = RequestMethod.POST
			, produces = { MediaType.APPLICATION_JSON_UTF8_VALUE }
			, consumes = { MediaType.APPLICATION_JSON_UTF8_VALUE })
	@ResponseBody
	public void postUsers(@RequestBody CreateUserReq req) throws Exception {
		log.debug("-- begin : postUsers --");
		if(usersService.existsById(req.getUsername())) {
			throw new Exception("username is exist");
		}else {
			usersService.createUser(req);
		}
	}
	
	@RequestMapping(path = "/users/orders", method = RequestMethod.POST
			, produces = { MediaType.APPLICATION_JSON_UTF8_VALUE }
			, consumes = { MediaType.APPLICATION_JSON_UTF8_VALUE })
	@ResponseBody
	public OrdersResp securePostUsersOrders(@RequestBody OrdersReq req) throws Exception {
		log.debug("-- begin : securePostUsersOrders --");
		return OrdersResp.builder()
				.price(ordersService.saveOrders(httpRequestUtil.getHttpHeaderUserId(), req.getOrders()))
				.build();
	}

	@RequestMapping(path = "/books", method = RequestMethod.GET
			, produces = { MediaType.APPLICATION_JSON_UTF8_VALUE }
			, consumes = { MediaType.APPLICATION_JSON_UTF8_VALUE })
	@ResponseBody
	public List<ExternalPubisherBookBean> getBooks() {
		log.debug("-- begin : getBooks --");
		return externalPubisherService.getAllBookPubisher();
	}

}
