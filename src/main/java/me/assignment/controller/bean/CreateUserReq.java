package me.assignment.controller.bean;

import java.io.Serializable;
import java.util.Date;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class CreateUserReq implements Serializable {
	/**
	* 
	*/
	private static final long serialVersionUID = 5959771184775150971L;
	
	private String username;
	
	private String password;
	
	@JsonProperty("date_of_birth")
	@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd/MM/yyyy")
	protected Date dateOfBirth;
}
