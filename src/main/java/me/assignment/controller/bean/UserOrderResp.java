package me.assignment.controller.bean;

import java.util.ArrayList;
import java.util.List;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class UserOrderResp extends UserResp {
	/**
	 * 
	 */
	private static final long serialVersionUID = -7780108249167615893L;

	@Builder(builderMethodName = "builderByUser")
	public UserOrderResp(UserResp user, List<Long> books) {
		super(user.getName(), user.getSurname(), user.getDateOfBirth());
		this.books = books;
	}

	private List<Long> books = new ArrayList<>();

}
