package me.assignment.controller.bean;

import java.io.Serializable;
import java.util.Date;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class LoginResp implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 2844537026930082834L;

	private String token;
	private Date date;

}
