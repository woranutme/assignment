package me.assignment.controller.bean;

import java.io.Serializable;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;

import lombok.Builder;
import lombok.Data;
import me.assignment.serializer.MoneySerializer;

@Data
@Builder
public class OrdersResp implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -5655612377748562044L;

	@JsonSerialize(using = MoneySerializer.class)
	private Double price;
}
