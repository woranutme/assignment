package me.assignment.controller.bean;

import java.io.Serializable;
import java.util.Date;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Builder
public class UserResp implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 3966523261078579755L;

	protected String name;

	protected String surname;

	@JsonProperty("date_of_birth")
	@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd/MM/yyyy", locale = "en_US")
	protected Date dateOfBirth;
}
