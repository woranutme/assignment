package me.assignment.controller.bean;

import java.io.Serializable;

import lombok.Data;

@Data
public class LoginReq implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 2703178090458875186L;
	
	private String username;
	private String password;

}
