package me.assignment.controller.bean;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import lombok.Data;

@Data
public class OrdersReq implements Serializable {/**
	 * 
	 */
	private static final long serialVersionUID = -972444612411456614L;
	
	private List<Long> orders = new ArrayList<>();

}
