package me.assignment.config.constant;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import lombok.Getter;

@Component
public class UserTokenConstants {

	@Getter
	@Value("${me.assignment.token.expiration}")
	private Long expirationToken;
}
