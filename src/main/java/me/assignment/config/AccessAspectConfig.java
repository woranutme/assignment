package me.assignment.config;

import java.util.Optional;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang3.StringUtils;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;

import io.jsonwebtoken.Claims;
import lombok.extern.log4j.Log4j2;
import me.assignment.config.constant.UserTokenConstants;
import me.assignment.exception.AopNotAuthorizedException;
import me.assignment.redis.model.UserToken;
import me.assignment.service.UserTokenService;
import me.assignment.utils.EncryptorUtil;
import me.assignment.utils.HttpRequestUtil;
import me.assignment.utils.JwtUtil;

@Aspect
@Configuration
@Log4j2
public class AccessAspectConfig {

	@Autowired
	private HttpRequestUtil httpRequestUtil;
	
	@Autowired
	private JwtUtil jwtUtil;

	@Autowired
	private EncryptorUtil encryptorUtil;

	@Autowired
	private UserTokenService userTokenService;
	
	@Autowired
	private UserTokenConstants userTokenConstants;

	@Before("execution(* me.assignment.controller.*.secure*(..))")
	public void before(JoinPoint joinPoint) {
		try {
			HttpServletRequest request = httpRequestUtil.getHttpServletRequest();
			String authorization = request.getHeader(HttpRequestUtil.AUTHORIZATION);
			String userId = request.getHeader(HttpRequestUtil.USER_ID);
			if (StringUtils.isBlank(authorization) || StringUtils.isBlank(userId)) {
				throw new AopNotAuthorizedException("Authorization Invalid[100]");
			}
			Claims claims = jwtUtil.parseJWT(authorization);
			log.debug("jwtUtil id: {}", claims.getId());
			if (!StringUtils.equals(userId, claims.getIssuer())) {
				throw new AopNotAuthorizedException("Authorization Invalid[102]");
			}
			if (!StringUtils.startsWith(encryptorUtil.decrypt(claims.getId()), userId)) {
				throw new AopNotAuthorizedException("Authorization Invalid[103]");
			}
			Optional<UserToken> redisToken = userTokenService.findById(claims.getId());
			if (redisToken.isPresent()) {
				redisToken.get().setExpiration(userTokenConstants.getExpirationToken());
				userTokenService.save(redisToken.get());
			}else {
				throw new AopNotAuthorizedException("Authorization Invalid[101]");
			}
		} catch (AopNotAuthorizedException ex) {
			throw ex;
		} catch (Exception e) {
			log.debug("error Authorization", e);
			throw new AopNotAuthorizedException("Authorization Invalid");
		}
	}
}
