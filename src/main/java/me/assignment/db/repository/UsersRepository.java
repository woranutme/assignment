package me.assignment.db.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import me.assignment.db.entity.UsersEntity;

@Repository
public interface UsersRepository extends JpaRepository<UsersEntity, String> {
}