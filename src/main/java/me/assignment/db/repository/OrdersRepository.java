package me.assignment.db.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import me.assignment.db.entity.OrdersEntity;

public interface OrdersRepository extends JpaRepository<OrdersEntity, Long> {
}
