package me.assignment.db.entity;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
@Entity(name = "USERS")
public class UsersEntity implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 5572639670267838771L;

	@Column(name = "USERNAME", nullable = false)
	@Id
	private String username;
	
	@Column(name = "name", nullable = false)
	private String name;
	
	@Column(name = "SURNAME", nullable = false)
	private String surname;

	@Column(name = "PASSWORD", nullable = false)
	private String password;

	@Temporal(TemporalType.DATE)
	@Column(name = "DATE_OF_BIRTH", nullable = false)
	private Date dateOfBirth;

	@OneToMany(cascade = CascadeType.DETACH, fetch = FetchType.LAZY, mappedBy = "usernameOrder")
	private List<OrdersEntity> orderList = new ArrayList<>();
}
