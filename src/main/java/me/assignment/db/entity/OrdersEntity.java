package me.assignment.db.entity;

import java.io.Serializable;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
@Entity(name = "ORDERS")
public class OrdersEntity implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 3475455740059641676L;

	@Id
	@Column(name = "id", nullable = false)
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;

	@Column(name = "BOOK_ID", nullable = false)
	private Long bookId;

	@ManyToOne(fetch = FetchType.LAZY, cascade = CascadeType.DETACH)
	@JoinColumn(name = "USERNAME_ORDER", nullable = false, referencedColumnName = "username")
	private UsersEntity usernameOrder;
}
