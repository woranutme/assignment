package me.assignment.utils;

import javax.servlet.http.HttpServletRequest;

import org.springframework.stereotype.Component;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

@Component
public class HttpRequestUtil {
	
	public final static String AUTHORIZATION = "Authorization";

	public final static String USER_ID = "user-id";
	
	public HttpServletRequest getHttpServletRequest() {
		return ((ServletRequestAttributes) RequestContextHolder.currentRequestAttributes())
				.getRequest();
	}
	
	public String getHttpHeaderUserId() {
		return this.getHttpServletRequest().getHeader(USER_ID);
	}
}
