package me.assignment.utils;

import java.security.Key;
import java.util.Date;

import javax.crypto.spec.SecretKeySpec;
import javax.xml.bind.DatatypeConverter;

import org.springframework.stereotype.Component;

import io.jsonwebtoken.Claims;
import io.jsonwebtoken.JwtBuilder;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;

@Component
public class JwtUtil {

	private static SignatureAlgorithm signatureAlgorithm = SignatureAlgorithm.HS256;

	private static String SECRET_KEY = "SECRET_KEY";
  
	private static Key signingKey = new SecretKeySpec(DatatypeConverter.parseBase64Binary(SECRET_KEY), signatureAlgorithm.getJcaName());
	
	public String createToken(String id, String issuer, String subject, long ttlMillis) {
	    long nowMillis = System.currentTimeMillis();
	    Date now = new Date(nowMillis);
		JwtBuilder builder = Jwts.builder().setId(id)
	            .setIssuedAt(now)
	            .setSubject(subject)
	            .setIssuer(issuer)
	            .signWith(signatureAlgorithm, signingKey);
	    if (ttlMillis > 0) {
	        long expMillis = nowMillis + ttlMillis;
	        Date exp = new Date(expMillis);
	        builder.setExpiration(exp);
	    }  
		return builder.compact();
	}
	
	public Claims parseJWT(String jwt) {
		return Jwts.parser()
				.setSigningKey(DatatypeConverter.parseBase64Binary(SECRET_KEY))
				.parseClaimsJws(jwt).getBody();
	}
	
	public boolean isSigned(String jwt) {
		return Jwts.parser()         
			       .setSigningKey(DatatypeConverter.parseBase64Binary(SECRET_KEY)).isSigned(jwt);
	}
}
