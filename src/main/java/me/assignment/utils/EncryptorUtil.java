package me.assignment.utils;

import java.io.UnsupportedEncodingException;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;

import javax.crypto.Cipher;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;

import org.apache.commons.codec.binary.Base64;
import org.springframework.stereotype.Component;

import lombok.extern.log4j.Log4j2;

@Log4j2
@Component
public class EncryptorUtil {

	private Cipher cipherEncrypt;
	private Cipher cipherDecrypt;

	private String key = "Bar12345Bar12345"; // 128 bit key
	private String initVector = "RandomInitVector"; // 16 bytes IV

	public EncryptorUtil() throws NoSuchAlgorithmException, NoSuchPaddingException, UnsupportedEncodingException,
			InvalidKeyException, InvalidAlgorithmParameterException {
		IvParameterSpec iv = new IvParameterSpec(initVector.getBytes("UTF-8"));
		SecretKeySpec skeySpec = new SecretKeySpec(key.getBytes("UTF-8"), "AES");
		cipherEncrypt = Cipher.getInstance("AES/CBC/PKCS5PADDING");
		cipherEncrypt.init(Cipher.ENCRYPT_MODE, skeySpec, iv);
		cipherDecrypt = Cipher.getInstance("AES/CBC/PKCS5PADDING");
		cipherDecrypt.init(Cipher.DECRYPT_MODE, skeySpec, iv);
	}

	public String encrypt(String value) throws Exception {
		log.debug("-- begin : encrypt value: {} --", value);
		try {
			byte[] encrypted = cipherEncrypt.doFinal(value.getBytes());
			System.out.println("encrypted string: " + Base64.encodeBase64String(encrypted));
			return Base64.encodeBase64String(encrypted);
		} catch (Exception ex) {
			log.error("encrypt error", ex);
			throw ex;
		}
	}

	public String decrypt(String value) throws Exception {
		log.debug("-- begin : decrypt value: {} --", value);
		try {
			return new String(cipherDecrypt.doFinal(Base64.decodeBase64(value)));
		} catch (Exception ex) {
			log.error("decrypt error", ex);
			throw ex;
		}
	}
}
