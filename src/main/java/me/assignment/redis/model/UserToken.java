package me.assignment.redis.model;

import java.io.Serializable;
import java.util.Date;

import org.springframework.data.annotation.Id;
import org.springframework.data.redis.core.RedisHash;
import org.springframework.data.redis.core.TimeToLive;

import lombok.Data;

@Data
@RedisHash(value="UserToken")
public class UserToken implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -4418189666190573828L;

	@Id
	private String id;
	
	@TimeToLive
	private Long expiration;
	
	private String username;
	
	private Date date;

}
