package me.assignment.redis.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import me.assignment.redis.model.UserToken;

@Repository
public interface UserTokenRepository extends CrudRepository<UserToken, String> {
}
