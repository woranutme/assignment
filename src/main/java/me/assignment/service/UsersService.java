package me.assignment.service;

import me.assignment.controller.bean.CreateUserReq;
import me.assignment.controller.bean.UserOrderResp;

public interface UsersService {

	public UserOrderResp getUserDetail(String username) throws Exception;
	
	public void deleteUserDetail(String username,String jwt) throws Exception;
	
	public boolean existsById(String username) throws Exception;
	
	public void createUser(CreateUserReq createUserReq) throws Exception;
}
