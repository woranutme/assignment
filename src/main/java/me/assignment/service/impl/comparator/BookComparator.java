package me.assignment.service.impl.comparator;

import java.util.Comparator;

import me.assignment.service.impl.bean.ExternalPubisherBookBean;

public class BookComparator implements Comparator<ExternalPubisherBookBean> {
	@Override
	public int compare(ExternalPubisherBookBean o1, ExternalPubisherBookBean o2) {
		int compareRec = o2.getIsRecommended().compareTo(o1.getIsRecommended());
		if (compareRec == 0) {
			return o1.getBookName().compareTo(o2.getBookName());
		} else {
			return compareRec;
		}

	}
}
