package me.assignment.service.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import lombok.extern.log4j.Log4j2;
import me.assignment.db.entity.OrdersEntity;
import me.assignment.db.entity.UsersEntity;
import me.assignment.db.repository.OrdersRepository;
import me.assignment.db.repository.UsersRepository;
import me.assignment.service.ExternalPubisherService;
import me.assignment.service.OrdersService;
import me.assignment.service.impl.bean.ExternalPubisherBookBean;

@Log4j2
@Service
@Transactional(rollbackFor = Exception.class)
public class OrdersServiceImpl implements OrdersService {

	@Autowired
	private OrdersRepository ordersRepository;

	@Autowired
	private UsersRepository usersRepository;

	@Autowired
	private ExternalPubisherService externalPubisherService;

	public Double saveOrders(String username, List<Long> orders) throws Exception {
		log.debug("-- begin : saveOrders --");
		Optional<UsersEntity> user = usersRepository.findById(username);
		if (!user.isPresent()) {
			throw new Exception("user not exist");
		} else {
			List<ExternalPubisherBookBean> books = externalPubisherService.getAllBookPubisher();
			List<OrdersEntity> dataIns = new ArrayList<>();
			Double price = 0D;
			for (Long bookId : orders) {
				ExternalPubisherBookBean bookEq = null;
				for (ExternalPubisherBookBean book : books) {
					if (book.getId().equals(bookId)) {
						bookEq = book;
						price += bookEq.getPrice();
						break;
					}
				}
				if (bookEq == null) {
					throw new Exception("book id not exist");
				}
				OrdersEntity order = new OrdersEntity();
				order.setBookId(bookId);
				order.setUsernameOrder(user.get());
				dataIns.add(order);
			}
			ordersRepository.saveAll(dataIns);
			return price;
		}
	}
}
