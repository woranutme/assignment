package me.assignment.service.impl;

import java.util.Date;
import java.util.Optional;

import org.apache.commons.lang3.RandomStringUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import io.jsonwebtoken.Claims;
import lombok.extern.log4j.Log4j2;
import me.assignment.config.constant.UserTokenConstants;
import me.assignment.db.entity.UsersEntity;
import me.assignment.db.repository.UsersRepository;
import me.assignment.exception.AopNotAuthorizedException;
import me.assignment.redis.model.UserToken;
import me.assignment.redis.repository.UserTokenRepository;
import me.assignment.service.UserTokenService;
import me.assignment.utils.EncryptorUtil;
import me.assignment.utils.JwtUtil;

@Log4j2
@Service
@Transactional(rollbackFor = Exception.class)
public class UserTokenServiceImpl implements UserTokenService {

	@Autowired
	private UserTokenConstants userTokenConstants;

	@Autowired
	private UserTokenRepository userTokenRepository;

	@Autowired
	private UsersRepository userRepository;

	@Autowired
	private EncryptorUtil encryptorUtil;

	@Autowired
	private JwtUtil jwtUtil;

	public Optional<UserToken> findById(String id) {
		return userTokenRepository.findById(id);
	}

	public UserToken save(UserToken entity) {
		return userTokenRepository.save(entity);
	}

	public String saveToken(String username, String password) throws Exception {
		log.debug("-- begin : saveToken : {},{} --", username, password);
		if (StringUtils.isBlank(username) || StringUtils.isBlank(password)) {
			throw new AopNotAuthorizedException("please input username and password");
		}
		Optional<UsersEntity> user = userRepository.findById(username);
		if (user.isPresent()) {
			if (StringUtils.equals(password, user.get().getPassword())) {
				String token = username + "_" + RandomStringUtils.random(16, true, true);
				String tokenEncrypt = encryptorUtil.encrypt(token);
				String jwt = jwtUtil.createToken(tokenEncrypt, username, "login", 0);
				UserToken userToken = new UserToken();
				userToken.setId(tokenEncrypt);
				userToken.setUsername(username);
				userToken.setDate(new Date());
				userToken.setExpiration(userTokenConstants.getExpirationToken());
				userTokenRepository.save(userToken);
				return jwt;
			} else {
				throw new AopNotAuthorizedException("username or password invalid");
			}
		} else {
			throw new AopNotAuthorizedException("username or password invalid");
		}
	}

	public void deleteTokenByJwt(String Jwt) throws Exception {
		log.debug("-- begin : deleteTokenByJwt --");
		Claims data = jwtUtil.parseJWT(Jwt);
		userTokenRepository.deleteById(data.getId());

	}
}
