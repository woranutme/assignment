package me.assignment.service.impl;

import java.util.Optional;
import java.util.stream.Collectors;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import lombok.extern.log4j.Log4j2;
import me.assignment.controller.bean.CreateUserReq;
import me.assignment.controller.bean.UserOrderResp;
import me.assignment.controller.bean.UserResp;
import me.assignment.db.entity.UsersEntity;
import me.assignment.db.repository.OrdersRepository;
import me.assignment.db.repository.UsersRepository;
import me.assignment.service.UserTokenService;
import me.assignment.service.UsersService;

@Log4j2
@Service
@Transactional(rollbackFor = Exception.class)
public class UsersServiceImpl implements UsersService {

	@Autowired
	private UsersRepository usersRepository;
	
	@Autowired
	private OrdersRepository ordersRepository;
	
	@Autowired
	private UserTokenService userTokenService;
	
	public UserOrderResp getUserDetail(String username) throws Exception {
		log.debug("-- begin : getUserDetail --");
		Optional<UsersEntity> user = usersRepository.findById(username);
		if(user.isPresent()) {
			return UserOrderResp.builderByUser()
					.user(UserResp.builder()
							.name(user.get().getName())
							.surname(user.get().getSurname())
							.dateOfBirth(user.get().getDateOfBirth())
							.build())
					.books(user.get().getOrderList().stream().map(u->u.getBookId()).collect(Collectors.toList()))
					.build();
		}else {
			return null;
		}
	}
	
	public void deleteUserDetail(String username,String jwt) throws Exception {
		log.debug("-- begin : deleteUserDetail --");
		Optional<UsersEntity> user = usersRepository.findById(username);
		if(user.isPresent()) {
			if(!user.get().getOrderList().isEmpty()) {
				ordersRepository.deleteAll(user.get().getOrderList());
			}
			usersRepository.delete(user.get());
			userTokenService.deleteTokenByJwt(jwt);
		}
	}
	
	public boolean existsById(String username) throws Exception {
		log.debug("-- begin : existsById --");
		return usersRepository.existsById(username);
	}
	
	public void createUser(CreateUserReq createUserReq) throws Exception {
		log.debug("-- begin : createUser --");
		if(StringUtils.isBlank(createUserReq.getUsername())
			|| StringUtils.isBlank(createUserReq.getUsername())
			|| createUserReq.getDateOfBirth() == null) {
			throw new Exception("data invalid");
		}else if(!StringUtils.contains(createUserReq.getUsername(), ".")){
			throw new Exception("username require .(dot) example: name.surname");
		}else {
			String[] usernameSplit = StringUtils.split(createUserReq.getUsername(), ".", 2);
			UsersEntity newUser = new UsersEntity();
			newUser.setUsername(createUserReq.getUsername());
			newUser.setName(usernameSplit[0]);
			newUser.setSurname(usernameSplit[1]);
			newUser.setPassword(createUserReq.getPassword());
			newUser.setDateOfBirth(createUserReq.getDateOfBirth());
			usersRepository.save(newUser);
		}
	}
	
}
