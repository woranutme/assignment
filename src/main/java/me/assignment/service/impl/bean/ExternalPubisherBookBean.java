package me.assignment.service.impl.bean;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Data;

@Data
public class ExternalPubisherBookBean implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -3700798761241315185L;
	
	public ExternalPubisherBookBean() {
		this.isRecommended=false;
	}

	@JsonProperty("id")
	protected Long id;

	@JsonProperty("author_name")
	protected String authorName;

	@JsonProperty("book_name")
	protected String bookName;

	@JsonProperty("price")
	protected Double price;
	
	@JsonProperty("is_recommended")
	protected Boolean isRecommended;

}
