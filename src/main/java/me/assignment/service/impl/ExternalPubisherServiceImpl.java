package me.assignment.service.impl;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import com.google.gson.Gson;

import lombok.extern.log4j.Log4j2;
import me.assignment.service.ExternalPubisherService;
import me.assignment.service.impl.bean.ExternalPubisherBookBean;
import me.assignment.service.impl.bean.ExternalPubisherBookRecomendBean;
import me.assignment.service.impl.comparator.BookComparator;

@Log4j2
@Service
public class ExternalPubisherServiceImpl implements ExternalPubisherService {

	private static String PUBISHER_BOOK_URL = "https://scb-test-book-publisher.herokuapp.com/books";
	private static String PUBISHER_BOOK_RECOMMENDATION_URL = "https://scb-test-book-publisher.herokuapp.com/books/recommendation";
	
	public List<ExternalPubisherBookBean> getAllBookPubisher() {
		log.debug("-- begin : getAllBookPubisher --");
		List<ExternalPubisherBookBean> data = this.getBookRecommendationPubisher();
		for (ExternalPubisherBookBean book : this.getBookPubisher()) {
			if(!data.stream().filter(d -> d.getId().equals(book.getId())).findFirst().isPresent()) {
				data.add(book);
			}
		}
		Collections.sort(data, new BookComparator());
		log.debug("=All=> "+ new Gson().toJson(data));
		return data;
	}
	
	public List<ExternalPubisherBookBean> getBookPubisher() {
		log.debug("-- begin : getBookPubisher --");
		RestTemplate restTemplate = new RestTemplate();
		ResponseEntity<ExternalPubisherBookBean[]> response 
			= restTemplate.getForEntity(PUBISHER_BOOK_URL, ExternalPubisherBookBean[].class);
		log.debug("==> "+ new Gson().toJson(response.getBody()));
		return response.hasBody() ? new ArrayList<ExternalPubisherBookBean>(Arrays.asList(response.getBody())) : Collections.emptyList();
	}
	
	public List<ExternalPubisherBookBean> getBookRecommendationPubisher() {
		log.debug("-- begin : getBookRecommendationPubisher --");
		RestTemplate restTemplate = new RestTemplate();
		ResponseEntity<ExternalPubisherBookRecomendBean[]> response 
			= restTemplate.getForEntity(PUBISHER_BOOK_RECOMMENDATION_URL, ExternalPubisherBookRecomendBean[].class);
		log.debug("==> "+ new Gson().toJson(response.getBody()));
		return response.hasBody() ? new ArrayList<ExternalPubisherBookBean>(Arrays.asList(response.getBody())) : Collections.emptyList();
	}
	
}
