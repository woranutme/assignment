package me.assignment.service;

import java.util.List;

public interface OrdersService {

	public Double saveOrders(String username, List<Long> orders) throws Exception;
}
