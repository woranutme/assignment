package me.assignment.service;

import java.util.Optional;

import me.assignment.redis.model.UserToken;

public interface UserTokenService {

	public Optional<UserToken> findById(String id);
	
	public UserToken save(UserToken entity);

	public String saveToken(String username, String password) throws Exception;
	
	public void deleteTokenByJwt(String Jwt) throws Exception;
}
