package me.assignment.service;

import java.util.List;

import me.assignment.service.impl.bean.ExternalPubisherBookBean;

public interface ExternalPubisherService {

	public List<ExternalPubisherBookBean> getAllBookPubisher();
	
	public List<ExternalPubisherBookBean> getBookPubisher();
	
	public List<ExternalPubisherBookBean> getBookRecommendationPubisher();
}
